Social network test task deploy instructions
============================================

Requires Linux shell, Git, PostgreSQL, Python 3, Virtualenv

Clone project
-------------

    git clone https://bitbucket.org/lostclus/socialtest.git
    cd socialtest

Setup database
--------------

    sudo -u postgres psql
    create role socialtest with login createdb password 'socialtest';
    create database socialtest owner socialtest;

Deploy Django web application
-----------------------------

    virtualenv --python=python3 ve
    ve/bin/pip install -r requirements.txt
    ve/bin/python socialtest/manage.py migrate

Deploy test bot
---------------

    virtualenv --python=python3 botve
    botve/bin/pip install -r requirements-bot.txt

Run local server
----------------

    export CLEARBIT_KEY=<your_clearbit_key>
    ve/bin/python socialtest/manage.py runserver 127.0.0.1:8000

Run test bot
------------

    botve/bin/python socialtestbot.py --config socialtestbot.yaml

Note: if you don't want to cleanup generated data (users and posts) pass
--no-cleanup option
