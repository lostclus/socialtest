import sys
import argparse
import random

import yaml
import requests


parser = argparse.ArgumentParser(description='Bot to test socialtest API.')
parser.add_argument('--api-base-url',
                    metavar='URL',
                    dest='api_base_url',
                    default='http://localhost:8000/',
                    help='API base URL')
parser.add_argument('--number-of-users',
                    metavar='NUMBER',
                    type=int,
                    dest='number_of_users',
                    default=10,
                    help='Number of users to singup')
parser.add_argument('--max-posts-per-user',
                    metavar='NUMBER',
                    type=int,
                    dest='max_posts_per_user',
                    default=10,
                    help='Maximum number of posts per user')
parser.add_argument('--max-likes-per-user',
                    metavar='NUMBER',
                    type=int,
                    dest='max_likes_per_user',
                    default=10,
                    help='Maximum number of likes per user')
parser.add_argument('--config',
                    metavar='FILE',
                    dest='config',
                    default=None,
                    help='Read defaults from specified YAML config file')
parser.add_argument('--no-cleanup',
                    dest='cleanup',
                    default=True,
                    action='store_false',
                    help="Don't finally perform cleanup of users and posts")


def signup_users(number_of_users, api_base_url):
    """
    Signups number_of_users users and returns list of users as list of dicts
    """
    users = []

    for i in range(1, number_of_users + 1):
        username = 'user%d' % i
        password = 'passwd%04d' % i

        print("Signing-up user %s..." % username, end='', flush=True)
        response = requests.post(
            '%s/users/' % api_base_url.rstrip('/'),
            json={
                'username': username,
                'email': '%s@example.com' % username,
                'password': password,
            })
        response.raise_for_status()
        user = response.json()
        print("done")

        print("Signing-in user %s..." % username, end='', flush=True)
        response = requests.post(
            '%s/api-token-auth/' % api_base_url.rstrip('/'),
            json={
                'username': username,
                'password': password,
            })
        response.raise_for_status()
        user.update({'token': response.json()['token']})
        print("done")
        
        users.append(user)

    return users


def create_posts(user, max_posts_per_user, api_base_url):
    """
    Creates random number of posts owned by user up to max_posts_per_user. The
    user argument is user dict. Returns a list of created posts as list of
    dicts
    """
    number_of_posts = random.randint(0, max_posts_per_user)
    posts = []

    for i in range(1, number_of_posts + 1):
        print("Creating %d-th post of user %s..." % (i, user['username']),
              end='', flush=True)
        response = requests.post(
            '%s/posts/' % api_base_url.rstrip('/'),
            headers={'Authorization': 'JWT %s' % user['token']},
            json={
                'text': "Hi, this is my %d-th post. Best regards, %s " % (
                    i, user['username']),
            },
        )
        response.raise_for_status()
        post = response.json()
        print("done")

        posts.append(post)

    return posts


def like_posts(user, posts, max_likes_per_user, api_base_url):
    """
    Likes random number of posts from posts up to max_likes_per_user. The user
    argument is user dict, The posts argument is list of posts as list of dicts
    """
    number_of_likes = random.randint(0, max_likes_per_user)

    for i in range(1, number_of_likes + 1):
        post = random.choice(posts)
        print("User %s liking post %s..." % (user['username'], post['url']),
              end='', flush=True)
        response = requests.get(
            post['url'],
            headers={'Authorization': 'JWT %s' % user['token']},
        )
        response.raise_for_status()
        print("done")


def cleanup(users, posts):
    """
    Deletes users and posts
    """
    users_d = {x['url']: x for x in users}

    for post in posts:
        owner = users_d[post['owner']]
        print("Cleaning-up post of user %s..." % owner['username'], end='', flush=True)
        response = requests.delete(
            post['url'],
            headers={'Authorization': 'JWT %s' % owner['token']})
        response.raise_for_status()
        print("done")

    for user in users:
        print("Cleaning-up user %s..." % user['username'], end='', flush=True)
        response = requests.delete(
            user['url'],
            headers={'Authorization': 'JWT %s' % user['token']})
        response.raise_for_status()
        print("done")


def main(argv=None):

    if argv is None:
        argv = sys.argv
    args = parser.parse_args(argv[1:])

    if args.config:
        with open(args.config) as f:
            config = yaml.load(f, Loader=yaml.Loader)
        parser.set_defaults(**config)
        args = parser.parse_args(argv[1:])

    users = signup_users(args.number_of_users, args.api_base_url)
    posts = []
    try:
        for user in users:
            posts.extend(create_posts(user, args.max_posts_per_user,
                                      args.api_base_url))
        for user in users:
            like_posts(user, posts, args.max_likes_per_user, args.api_base_url)

    finally:
        if args.cleanup:
            # good to clean up after themselves
            cleanup(users, posts)

    print("All work is done, have a nice day!")


if __name__ == '__main__':
    main()
