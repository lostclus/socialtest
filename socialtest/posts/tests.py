from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.test import APIClient

from users.models import UserProfile
from posts.models import Post


class UserApiTest(TestCase):

    def setUp(self):

        self.user = User.objects.create_user(
            username='testuser',
            email='testuser@example',
            password='passwd12')
        UserProfile.objects.create(user=self.user)

    def test_create_and_detail(self):

        client = APIClient()

        response = client.post(
            '/posts/',
            {
                'text': 'test',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 401)

        client.login(username='testuser', password='passwd12')
        response = client.post(
            '/posts/',
            {
                'text': 'test text',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 201)
        post_id = int(response.data['url'].rstrip('/').split('/')[-1])

        response = client.get(response.data['url'], format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                'url': 'http://testserver/posts/%d/' % post_id,
                'text': 'test text',
                'likes': [],
                'owner': 'http://testserver/users/%d/' % self.user.pk,
            },
        )

    def test_like(self):

        client = APIClient()

        post = Post.objects.create(
            text='test text',
            owner=self.user)
        self.assertEqual(post.likes.count(), 0)

        client.login(username='testuser', password='passwd12')
        response = client.get('/posts/%d/like/' % post.pk, format='json')
        self.assertEqual(response.status_code, 200)

        self.assertEqual(post.likes.count(), 1)
        self.assertTrue(post.likes.filter(pk=self.user.pk).exists())

    def test_unlike(self):

        client = APIClient()

        post = Post.objects.create(
            text='test text',
            owner=self.user)
        post.likes.add(self.user)
        self.assertEqual(post.likes.count(), 1)

        client.login(username='testuser', password='passwd12')
        response = client.get('/posts/%d/unlike/' % post.pk, format='json')
        self.assertEqual(response.status_code, 200)

        self.assertEqual(post.likes.count(), 0)
