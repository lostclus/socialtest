from rest_framework import serializers

from posts.models import Post


class PostSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.HyperlinkedRelatedField(
        read_only=True, view_name='user-detail')

    class Meta:
        model = Post
        fields = ['url', 'text', 'likes', 'owner']
