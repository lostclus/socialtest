from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    text = models.TextField()
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='posts')
    likes = models.ManyToManyField(User, blank=True)
