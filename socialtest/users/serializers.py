from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework import fields

from users.models import UserProfile


class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserProfile
        fields = ['company']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = fields.CharField(required=False, write_only=True)
    profile = UserProfileSerializer(required=False)

    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'password', 'profile']

    def create(self, validated_data):
        profile_data = validated_data.pop('profile', {})
        user = User.objects.create_user(**validated_data)
        profile = UserProfile.objects.create(
            user=user, **profile_data)
        return user
