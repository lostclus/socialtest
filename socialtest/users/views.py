from django.contrib.auth.models import User

from rest_framework import permissions
from rest_framework import viewsets

from users.serializers import UserSerializer


class AllowAnyCreate(permissions.BasePermission):
    """
    Allow any create
    """

    def has_permission(self, request, view):
        return request.method == 'POST'


class IsSelf(permissions.BasePermission):
    """
    Allow access to self
    """
    def has_object_permission(self, request, view, obj):
        return request.user.is_authenticated and obj == request.user


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [AllowAnyCreate | IsSelf]

    def get_queryset(self):

        if not self.request.user.is_authenticated:
            return self.queryset.none()

        queryset = super(UserViewSet, self).get_queryset()

        if self.request.user.is_staff:
            return queryset

        return queryset.filter(pk=self.request.user.pk)
