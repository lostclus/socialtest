import unittest

from django.test import TestCase
from django.conf import settings
from django.contrib.auth.models import User

from rest_framework.test import APIClient


class UserApiTest(TestCase):

    def test_create_and_detail(self):

        client = APIClient()

        response = client.post(
            '/users/',
            {
                'username': 'testuser',
                'email': 'testuser@example.com',
                'profile': {
                    'company': 'Test company',
                },
                'password': 'passwd12',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 201)

        user_id = int(response.data['url'].rstrip('/').split('/')[-1])
        user = User.objects.get(pk=user_id)
        self.assertTrue(user.check_password('passwd12'))

        client.login(username='testuser', password='passwd12')
        response = client.get(response.data['url'], format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                'url': 'http://testserver/users/%d/' % user_id,
                'username': 'testuser',
                'email': 'testuser@example.com',
                'profile': {
                    'company': 'Test company',
                },
            },
        )

    @unittest.skipIf(not settings.CLEARBIT_KEY, "CLEARBIT_KEY is not set")
    def test_create_autodetect_company(self):

        client = APIClient()

        response = client.post(
            '/users/',
            {
                'username': 'testuser',
                'email': 'alex@clearbit.com',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 201)

        user_id = int(response.data['url'].rstrip('/').split('/')[-1])
        user = User.objects.get(pk=user_id)
        self.assertTrue(user.profile.company, 'Clearbit')


    def test_list(self):

        client = APIClient()

        response = client.post(
            '/users/',
            {
                'username': 'testuser1',
                'email': 'testuser1@example.com',
                'password': 'passwd12',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 201)
        user1_id = int(response.data['url'].rstrip('/').split('/')[-1])

        response = client.post(
            '/users/',
            {
                'username': 'testuser2',
                'email': 'testuser1@example.com',
            },
            format='json',
        )
        self.assertEqual(response.status_code, 201)

        response = client.get('/users/', format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

        client.login(username='testuser1', password='passwd12')

        response = client.get('/users/', format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [
            {
                'url': 'http://testserver/users/%d/' % user1_id,
                'username': 'testuser1',
                'email': 'testuser1@example.com',
                'profile': {
                    'company': '',
                },
            },
        ])
