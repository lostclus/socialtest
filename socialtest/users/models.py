import clearbit
from requests.exceptions import HTTPError

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

import setupclearbit


class UserProfile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name='profile')
    company = models.CharField(max_length=200)

    def save(self, **kwargs):

        if (clearbit.key and not self.company and
            self.user_id and self.user.email):
            try:
                response = clearbit.Enrichment.find(
                    email=self.user.email, stream=True, timeout=3)
            except HTTPError:
                pass
            else:
                # buggy implementation of response class that redefines
                # dict.get method, so we implicitly convert this to dict
                response = dict(response)
                if response.get('company', {}).get('name'):
                    self.company = response['company']['name']

        super(UserProfile, self).save(**kwargs)
