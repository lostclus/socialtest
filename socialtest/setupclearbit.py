import clearbit
import clearbit.resource

from django.conf import settings


clearbit.key = settings.CLEARBIT_KEY

if 'timeout' not in clearbit.resource.Resource.valid_options:
    # Add timeout to valid options to every clearbit resource, so that it can
    # by passed to requests library call
    clearbit.resource.Resource.valid_options.append('timeout')
