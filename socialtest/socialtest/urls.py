from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url

from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

import users.views
import posts.views


router = routers.DefaultRouter()
router.register(r'users', users.views.UserViewSet)
router.register(r'posts', posts.views.PostViewSet)


urlpatterns = [
    path('', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^api-token-auth/', obtain_jwt_token),
    path('admin/', admin.site.urls),
    #url(r'^accounts/', include('django_registration.backends.activation.urls')),
    url(r'^accounts/', include('django.contrib.auth.urls')),
]
